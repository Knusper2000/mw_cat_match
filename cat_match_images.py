import re
import math as m
from glob import glob as g
from copy import copy
from os import path

from astropy.io import fits
from astropy.table import Table
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.nddata import Cutout2D
from astropy.visualization import ImageNormalize, AsinhStretch
from astropy import units as u

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

from PIL import Image, ImageDraw, ImageFont  # https://python-pillow.org/


# parameters 
output_dir = '/scratch/home/eherenz/work/MW_cat_match/mw_cat_match_imgs/'
hlsp_hlf_v20_cat_file = '/scratch/home/eherenz/work/MW_cat_match/hlsp_hlf_v2.0_catalogs/hlsp_hlf_hst_60mas_goodss_v2.0_catalog.fits'
mwdr1_catfile = '/scratch/home/eherenz/work/MW_cat_match/mw_dr1_tabs/MW_44fields_main_table_v1.0.fits'
hlf_images_dir = '/scratch/home/eherenz/work/MW_cat_match/60mas_v2.0/'
fontpath = '/scratch/home/eherenz/.fonts/Hack-Regular.ttf'  # https://sourcefoundry.org/hack/

#  arbitary values for the shape params if not defined in hlsp cat    
a_arb = 5; b_arb = 5; angle_arb = 0; kron_arb = 0  

cutout_size = 6.5 * u.arcsec
match_radius = 0.5 * u.arcsec 

# HLF catalogue (v2.0)
hlsp_hlf_v20_cat = Table.read(hlsp_hlf_v20_cat_file)
hlsp_id = hlsp_hlf_v20_cat['id']
hlsp_ra = hlsp_hlf_v20_cat['ra']
hlsp_dec = hlsp_hlf_v20_cat['dec']
hlsp_a_image = hlsp_hlf_v20_cat['a_image']
hlsp_b_image = hlsp_hlf_v20_cat['b_image']
hlsp_theta = hlsp_hlf_v20_cat['theta_J2000']
hlsp_kronr = hlsp_hlf_v20_cat['kron_radius']
# some magnitudes from the HLF catalogue (zeropoint = 25. mag)
m336_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f336w']) + 25.
sn336_w = hlsp_hlf_v20_cat['f_f336w'] / hlsp_hlf_v20_cat['e_f336w']
m435_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f435w']) + 25.
sn435_w = hlsp_hlf_v20_cat['f_f435w'] / hlsp_hlf_v20_cat['e_f435w']
m606_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f606w']) + 25.
sn606_w = hlsp_hlf_v20_cat['f_f606w'] / hlsp_hlf_v20_cat['e_f606w']
m775_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f775w']) + 25.
sn775_w = hlsp_hlf_v20_cat['f_f775w'] / hlsp_hlf_v20_cat['e_f775w']
m814_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f814w']) + 25.
sn814_w = hlsp_hlf_v20_cat['f_f814w'] / hlsp_hlf_v20_cat['e_f814w']
m850_lp = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f850lp']) + 25.
sn850_lp = hlsp_hlf_v20_cat['f_f850lp'] / hlsp_hlf_v20_cat['e_f850lp']
m105_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f105w']) + 25.
sn105_w = hlsp_hlf_v20_cat['f_f105w'] / hlsp_hlf_v20_cat['e_f105w']
m125_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f125w']) + 25.
sn125_w = hlsp_hlf_v20_cat['f_f125w'] / hlsp_hlf_v20_cat['e_f125w']
m160_w = -2.5 * np.log10(hlsp_hlf_v20_cat['f_f160w']) + 25.
sn160_w = hlsp_hlf_v20_cat['f_f160w'] / hlsp_hlf_v20_cat['e_f160w']
# ... this super catalog stores -99 in flux and errors for failed
# measurements, so magically they have S/N = 1 ... how professional...
for mag,sn in [(m336_w, sn336_w), (m435_w, sn435_w), (m606_w, sn606_w), (m775_w, sn775_w),
               (m814_w, sn814_w), (m850_lp, sn850_lp), (m105_w, sn105_w), (m125_w, sn125_w),
               (m160_w, sn160_w)]:
    mag_nan_sel = np.isnan(mag)
    sn[mag_nan_sel] = 0.

# MW catalogue
mwdr1_cat = Table.read(mwdr1_catfile)
mw_id = mwdr1_cat['UNIQUE_ID']
mw_ra = mwdr1_cat['RA'] 
mw_dec = mwdr1_cat['DEC']
mw_sn = mwdr1_cat['SN']
mw_redshift = mwdr1_cat['Z']
mw_confidence = mwdr1_cat['CONFIDENCE']
    
im_norm = ImageNormalize(vmin=-0.005, vmax=0.1, stretch=AsinhStretch())

# Sky coordinates and associated HLF sources to MUSE sources within
# the region of the cutouts (+ some margin, thus 2 * cutout_size)
mw_sky_coord = SkyCoord(mw_ra, mw_dec)
hlsp_sky_coord = SkyCoord(hlsp_ra * u.deg, hlsp_dec * u.deg)
idx_hlsp, idx_mw, d2d, d3d = mw_sky_coord.search_around_sky(hlsp_sky_coord, 2 * cutout_size)

# HLF image
hlsp_image_filelist = g(hlf_images_dir + '/*sci.fits')
hlsp_image_filelist.sort()

def label(xy, text, size=14., shift_x=0.0, shift_y=0.0, color='cyan',
          align_x='center', align_y='center', axis=None):
    x = xy[0] + shift_x
    y = xy[1] + shift_y
    if ax==None:
        txt = plt.text(x, y, text, ha=align_x, va=align_y,
                       family='sans-serif', size=size, color=color)
    else:
        txt = axis.text(x, y, text, ha=align_x, va=align_y,
                        family='sans-serif', size=size, color=color)
    return txt


for hlsp_image_file in hlsp_image_filelist:
    hlsp_image = fits.getdata(hlsp_image_file)
    hlsp_image_head = fits.getheader(hlsp_image_file)
    try:
        # some frames come with GAIA astrometry, but store the old GOODS-S
        # WCS is second WCS (primary is GAIA)
        hlsp_image_wcs = WCS(hlsp_image_head, key='A')
    except KeyError:
        # if not it defaults to old GOODS-S
        hlsp_image_wcs = WCS(hlsp_image_head)

    # generate band name string from filename via regex
    band_name = re.search('goodss_(.+?)_v', hlsp_image_file).group(1)
    band_name = band_name.upper()


    text_gen = False
    for mw_id_i,  mw_sky_coord_i, mw_redshift_i, mw_sn_i, mw_confidence_i in zip(mw_id,  mw_sky_coord, mw_redshift,
                                                                                 mw_sn, mw_confidence):
        # create the cutout for each MW source
        mw_cutout = Cutout2D(hlsp_image, mw_sky_coord_i, cutout_size,
                             wcs=hlsp_image_wcs,  mode='partial', fill_value=0.)

        # selected associated sources from HLF for plotting
        idx_hlsp_i = idx_hlsp[mw_id[idx_mw] == mw_id_i]
        d2d_i = d2d[mw_id[idx_mw] == mw_id_i]  # distances to the MW source
               
        # plot the cutouts ...
        fig = plt.figure(figsize=(4,4))  # the main figure
        fig_copy = plt.figure(figsize=(4,4)) # figure without the artists
        ax = fig.add_subplot(projection=mw_cutout.wcs)
        ax_copy = fig_copy.add_subplot(projection=mw_cutout.wcs)
        im = ax.imshow(mw_cutout.data, norm=im_norm, cmap='cubehelix')
        im_copy = ax_copy.imshow(mw_cutout.data, norm=im_norm, cmap='cubehelix')
        
        # ... with all the HLSP the sources overlaid as ellipses
        for hlsp_sky_coord_ii, hlsp_a_image_ii, hlsp_b_image_ii, hlsp_theta_ii, hlsp_kronr_ii, hlsp_id_ii, d2d_ii in \
            zip(hlsp_sky_coord[idx_hlsp_i], hlsp_a_image[idx_hlsp_i], hlsp_b_image[idx_hlsp_i],
                hlsp_theta[idx_hlsp_i], hlsp_kronr[idx_hlsp_i], hlsp_id[idx_hlsp_i], d2d_i):
            x_ii,y_ii = mw_cutout.wcs.world_to_pixel(hlsp_sky_coord_ii)
            if hlsp_a_image_ii != -99.0:
                # matplotlib ellipse artist (reversed angle defintion in
                # matplotlib, as well coordinates refers to corner of pixel)
                el = Ellipse((x_ii+0.5, y_ii+0.5),
                             hlsp_a_image_ii * hlsp_kronr_ii,
                             hlsp_b_image_ii * hlsp_kronr_ii, -1 * hlsp_theta_ii)
                el.set_edgecolor('cyan')
                label_color = 'darkturquoise'
                label_shift_x = (hlsp_a_image_ii) * m.sin(m.radians(-hlsp_theta_ii))
                label_shift_y = (hlsp_a_image_ii) * m.cos(m.radians(-hlsp_theta_ii))
            else:
                # ugly green circle for hlsp source with no structural parameters
                # (note: of a is -99 all other ellipse paramters are also -99)
                el = Ellipse((x_ii+0.5, y_ii+0.5),
                              a_arb, b_arb, angle_arb) 
                el.set_edgecolor('cadetblue')
                label_color = 'cadetblue'
                label_shift_x = 0.
                label_shift_y = -5.
               
            el.set_facecolor('none')
            ax.add_artist(el)

            if label_shift_x > 0:
                align_x = 'left'
            elif label_shift_x == 0:
                align_x = 'center'
            else:
                align_x = 'right'

            if label_shift_y > 0:
                align_y = 'bottom'
            else:
                align_y = 'top'

            if x_ii > 0 and x_ii < mw_cutout.xmax_cutout and y_ii > 0 and y_ii < mw_cutout.ymax_cutout:
                txt_list = []
                for axi in [ax,ax_copy]:
                    txt = label((x_ii, y_ii), str(int(hlsp_id_ii)), size=10, color=label_color,
                                shift_x=label_shift_x,
                                shift_y=label_shift_y,
                                align_x=align_x,
                                align_y=align_y, axis = axi)
                    txt_list.append(txt)
                
            if d2d_ii <= match_radius:
                # draw fat ellipse or circle when source is within
                # match_radius for MUSE sources
                el.set_linewidth(3.)
                [txt.set_fontweight('bold') for txt in txt_list]

        # closest HST counterpart:
        if d2d_i.min() <= match_radius:
            hst_cpt = True
            hst_cpt_d = d2d_i.min()
        else:
            hst_cpt = False

        # label of the ACS/WFC band
        ax.set_title(band_name)
        ax_copy.set_title(band_name)

        # MUSE-Wide source gets a r=0.5'' (plate scale = 60 mas) circle.
        mw_obj_x, mw_obj_y = mw_cutout.wcs.world_to_pixel(mw_sky_coord_i)
        # we assume explicitly 60mas plate scale here, thus 8.3... pixel = 0.5 arcsec
        mw_obj_crc = Ellipse((mw_obj_x + 0.5, mw_obj_y + 0.5), 8.333, 8.333, 0)  
        mw_obj_crc.set_facecolor('none')
        mw_obj_crc.set_edgecolor('red')
        mw_obj_crc_c = copy(mw_obj_crc)
        ax.add_artist(mw_obj_crc)
        ax_copy.add_artist(mw_obj_crc_c)

        # no ticklabels
        ax.coords[0].set_ticklabel_visible(False)
        ax.coords[1].set_ticklabel_visible(False)
        ax_copy.coords[0].set_ticklabel_visible(False)
        ax_copy.coords[1].set_ticklabel_visible(False)

        output_name = output_dir + str(mw_id_i) + '_' + band_name + '.png'
        fig.savefig(output_name)
        fig_copy.savefig(output_name[:-4]+'_ns.png')
        plt.close(fig)
        plt.close(fig_copy)

        print(output_name + ' created')
        
        # generate the text for each MW source
        if text_gen == False:
            mw_id_i_str = 'MW ID ' + str(mw_id_i)
            mw_redshift_i_str = 'z_MW = ' + str(round(mw_redshift_i, 4))
            mw_confidence_i_str = 'MW conf = ' + str(mw_confidence_i)
            mw_sn_i_str = 'MW S/N = ' + str(round(mw_sn_i, 2))
            
            mw_sky_coord_i_str_prep = mw_sky_coord_i.to_string(style='hmsdms')
            mw_sky_coord_i_str_prep = mw_sky_coord_i_str_prep.split()
            mw_sky_coord_i_str = 'RA = ' + mw_sky_coord_i_str_prep[0] + 'Dec = ' + mw_sky_coord_i_str_prep[1]

            out_txt = open(output_dir + str(mw_id_i) + '.txt', 'w')
            out_txt.write(mw_id_i_str + ' -- ' + mw_confidence_i_str + '\n')
            out_txt.write(mw_redshift_i_str + ' -- ' + mw_sn_i_str + ' \n' + mw_sky_coord_i_str + '\n')

            # ... and the closest HST source
            if hst_cpt:
                hst_cpt_idx = idx_hlsp_i[d2d_i.argmin()]
                #hst_cpt_cat = hlsp_hlf_v20_cat[hst_cpt_idx]
                hst_cpt_id_str = 'HLF ID '+str(int(hlsp_id[hst_cpt_idx]))
                hst_cpt_d_str = 'd(HLF-MW) = '+str(hst_cpt_d.to(u.arcsec).round(2))
                hst_cpt_m435_str = 'm435W = '+str(round(m435_w[hst_cpt_idx], 2))
                hst_cpt_sn435_str = 'SN(435W) = '+str(round(sn435_w[hst_cpt_idx], 2))
                hst_cpt_m606_str = 'm606W = '+str(round(m606_w[hst_cpt_idx], 2))
                hst_cpt_sn606_str = 'SN(606W) = '+str(round(sn606_w[hst_cpt_idx], 2))
                hst_cpt_m775_str = 'm775W = '+str(round(m775_w[hst_cpt_idx], 2))
                hst_cpt_sn775_str = 'SN(775W) = '+str(round(sn775_w[hst_cpt_idx], 2))
                hst_cpt_m814_str = 'm814W = '+str(round(m814_w[hst_cpt_idx], 2))
                hst_cpt_sn814_str = 'SN(814W) = '+str(round(sn814_w[hst_cpt_idx], 2))
                hst_cpt_m850_str = 'm850LP = '+str(round(m850_lp[hst_cpt_idx], 2))
                hst_cpt_sn850_str = 'SN(850LP) = '+str(round(sn850_lp[hst_cpt_idx], 2))
                hst_cpt_m105_str = 'm105W = '+str(round(m105_w[hst_cpt_idx], 2))
                hst_cpt_sn105_str = 'SN(105W) = '+str(round(sn105_w[hst_cpt_idx], 2))
                hst_cpt_m125_str = 'm125W = '+str(round(m125_w[hst_cpt_idx], 2))
                hst_cpt_sn125_str = 'SN(125W) = '+str(round(sn125_w[hst_cpt_idx], 2))
                hst_cpt_m160_str = 'm160W = '+str(round(m160_w[hst_cpt_idx], 2))
                hst_cpt_sn160_str = 'SN(160W) = '+str(round(sn160_w[hst_cpt_idx], 2))

                out_txt.write(hst_cpt_id_str + ' -- ' + hst_cpt_d_str + '\n')
                out_txt.write(hst_cpt_m435_str + ' -- ' + hst_cpt_sn435_str + '\n')
                out_txt.write(hst_cpt_m606_str + ' -- ' + hst_cpt_sn606_str + '\n')
                out_txt.write(hst_cpt_m775_str + ' -- ' + hst_cpt_sn775_str + '\n')
                out_txt.write(hst_cpt_m814_str + ' -- ' + hst_cpt_sn814_str + '\n')
                out_txt.write(hst_cpt_m850_str + ' -- ' + hst_cpt_sn850_str + '\n')
                out_txt.write(hst_cpt_m105_str + ' -- ' + hst_cpt_sn105_str + '\n')
                out_txt.write(hst_cpt_m125_str + ' -- ' + hst_cpt_sn125_str + '\n')
                out_txt.write(hst_cpt_m160_str + ' -- ' + hst_cpt_sn160_str + '\n')
            else:
                # no closest counterpart
                out_txt.write('no cpt. d <= ' + str(match_radius.to(u.arcsec)))

            out_txt.close()

    # text has been generated...
    text_gen = True

# create the combined image
font = ImageFont.truetype(fontpath, size=22)
hg = font.getsize('hg')[1]  # small hack to get the line height
x = 1250 

thumbnail_filelist = g(output_dir + '?????????_*.png')

obj_ids_files = [path.basename(filename) for filename in thumbnail_filelist]
obj_ids_files_p = [filename.partition('_') for filename in obj_ids_files]
obj_ids = list(set([file_p[0] for file_p in obj_ids_files_p]))
obj_ids.sort()

obj_ids_filter = ['F225W','F275W', 'F336W','F435W','F606W','F775W','F814W','F850LP', 'F098M',
                  'F105W','F125W','F140W','F160W']

columns = 5
rows = 3
for obj_id in obj_ids:
    pil_im_list = [Image.open(output_dir + obj_id + '_' + obj_ids_filter_i + '.png')
                   for obj_ids_filter_i in obj_ids_filter]
    pil_im_list_ns = [Image.open(output_dir + obj_id + '_' + obj_ids_filter_i + '_ns.png')
                      for obj_ids_filter_i in obj_ids_filter]

    final_image = Image.new('RGB', (pil_im_list[0].width * columns,
                                    pil_im_list[0].height * rows),
                            color='white')
    final_image_ns = Image.new('RGB', (pil_im_list_ns[0].width * columns,
                                       pil_im_list_ns[0].height * rows),
                               color='white')

    
    for i,pil_im in enumerate(pil_im_list):
        final_image.paste(pil_im, ((i % columns) * pil_im_list[0].width,
                                   int(i / columns) * pil_im_list[0].width))
    for i,pil_im in enumerate(pil_im_list_ns):
        final_image_ns.paste(pil_im, ((i % columns) * pil_im_list_ns[0].width,
                                      int(i / columns) * pil_im_list_ns[0].width))

    d = ImageDraw.Draw(final_image)
    d_ns = ImageDraw.Draw(final_image_ns)
    
    textfile = open(output_dir + obj_id + '.txt', 'r')
    textlines = textfile.readlines()
    y = 850  # int(i / columns) * pil_im_list[0].width + 50 
    for line in textlines:
        d.text((x,y), line, fill='black', font=font)
        d_ns.text((x,y), line, fill='black', font=font)        
        y += hg
        
    image_out_name = output_dir + obj_id + '_montage_PIL.png'
    final_image.save(image_out_name)

    image_out_name_ns = output_dir + obj_id + '_montage_PIL_ns.png'
    final_image_ns.save(image_out_name_ns)
        
    print(image_out_name)
