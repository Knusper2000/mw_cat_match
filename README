# XMatch between MUSE-Wide emission line catalogue and Hubble Legacy Field data #

## Overview ##

The python script `cat_match_images.py` produces thumbnail images for
MUSE-Wide emission line selected galaxies (Urrutia et al. 2019, see
also Herenz et al. 2017) using the images of the [Hubble Legacy
Field](https://archive.stsci.edu/prepds/hlf/) (HLF).  The HLF project
and the data reduction is described in Illingworth et al. (2016). I
also overplot sources from the Hubble Legacy Field catalogue by
Whitaker et al. (2019).

The script creates thumbnail PNG images `IIIIIIIIII_BAND.png` and
`IIIIIIIIII_BAND_ns.png`, where `IIIIIIIIII` is the MUSE-Wide ID and
`BAND` refers to the HLF filter (F225W, F275W, F336W, F435W, F606W,
F775W, F814W, F850LP, F098M, F105W, F125W, F140W, and F160W).  While
images without the suffix `ns` do not overlay the elliptical
photometric Kron-like SExtractor apertures, the images without this
suffix include overlays of these apertures.  For all images the 60mas
plate scale of the HLF reduction is used. 

HLF source IDs (and apertures) are shown in bold for sources within
0.5 arscec of the MUSE-Wide position.

The script also produces a combined image of all bands for each MW
source. This summary images also display text with some of the
parameters from the MUSE-Wide catalogue.  If a cross match is found
within 0.5 arcsec from the HLF then these summary images also contain
some information regarding the closest HLF counterpart.  As an example
find below a combined image for the MUSE-Wide source 136041192 (a z~5 Lyα emitter).

[![136034182_montage_PIL.png](./mw_cat_match_imgs_example/136041192_montage_PIL_small.png)](./mw_cat_match_imgs_example/136041192_montage_PIL.png)

A complete set of thumbnails and combined images for the MUSE-Wide DR1
catalogue (Urrutia et al. 2019) can be downloaded as a gzipped
tar-ball under the following link (4 GB): [mw_cat_match_imgs.tar.gz](https://europeansouthernobservatory-my.sharepoint.com/:u:/g/personal/edmundchristian_herenz_eso_org/EfM-ParWhgZLqsJsy4t_GFgBQ9Dytp5bNe_L1a_LMOUakQ?e=e5dhgD)



## Requirements ##

In order to run the script the following additional python packages
must be present in your python environment:

* [astropy](https://www.astropy.org/) (3.2.3)
* [numpy](https://numpy.org/) (1.17.4)
* [matplotlib](https://matplotlib.org/) (3.1.1)
* [Pillow](https://python-pillow.org/) (6.2.1).

A [conda / astroconda](https://astroconda.readthedocs.io/en/latest/) environment is recommended.

## Setup ##

The following path variables (strings) at the beginning of the script must be
adjusted:

  * `output_dir`: Path where thumbnail images and combined images will be saved.
  * `hlsp_hlf_v20_cat_file`: Path to the fits file storing the HLF 2.0
    catalogue. This catalogue can be downloaded at <https://archive.stsci.edu/hlsps/hlf/v2.0/catalogs/hlsp_hlf_hst_60mas_goodss_v2.0_catalog.fits>
  * `mwdr1_catfile`: The main fits table of MUSE-Wide DR1 from <https://musewide.aip.de/files/dr1/MW_44fields_main_table_v1.0.fits> or from the [CDS](http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=J/A%2bA/624/A141/emobj) 
  * `hlf_images_dir`: Directory containing the HLF images drizzled to
    a 60mas plate scale. These images can be found at MAST:
    <http://archive.stsci.edu/hlsps/hlf/v2.0/60mas/> - for MUSE-Wide DR1
    only the `goodss` images are relevant, and for the thumbnail
    creation the `*sci.fits` images suffice.
  * `fontpath`: Path to the TrueType font for the final images.  I
    recommend <https://sourcefoundry.org/hack/> (Regular) for optimal
    readability.

## Known Issues ##

 * Warnings when running the script can be ignored. 
 * Bands F098M and F225 do not contain data over GOODS-S, except in
   the HUDF, so it is unnecessary to process them for MUSE-Wide.

## References ##

 * Herenz E. C., et al., 2017, A&A, 606, A12 - <https://doi.org/10.1051/0004-6361/201834656>
 * Illingworth G., et al., 2016, arXiv, [arXiv:1606.00841](https://arxiv.org/abs/1606.00841)
 * Urrutia T., et al., 2019, A&A, 624, A141 - <https://doi.org/10.1051/0004-6361/201834656>
 * Whitaker K. E., et al., 2019, ApJS, 244, 16 - <https://doi.org/10.3847/1538-4365/ab3853>

